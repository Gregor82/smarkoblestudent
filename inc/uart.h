/**
 * @file			uart.hstatic
 * @date			16.11.2017
 *	
 * @details
 */

#ifndef INC_UART_H_
#define INC_UART_H_

#include <stdbool.h>

/**
 * @brief BLE commands to update the App.
 */
typedef enum
{
	SET_BATTERY_SOC		= 0x01,		//!< Write all battery level to the App.
	SET_BATTERY_VOLTAGE	= 0x02,		//!< Write all battery voltage to the App.
	SET_BATTERY_CURRENT	= 0x03,		//!< Write all battery current to the App.
	SET_BATTERY_TEMP	= 0x04,		//!< Write all battery temperature to the App.

	SET_PULSE_DATA 		= 0x05,		//!< Write the HRM values to the App.
	SET_SPO2_DATA 		= 0x06,		//!< Write the SpO2 values to the App.

	SET_BODY_TEMP		= 0x07,		//!< Write the skin temperature to the App.
	SET_ENV_TEMP		= 0x08,		//!< Write the environment temperature to the App.
	SET_ENV_HUMIDITY	= 0x09,		//!< Write the humidity to the App.

	SET_HAS_SKIN_CONTACT	= 0x10,		//!< Write the acceleration z-axis value to the App.

	SET_GYRO			= 0x1A,		//!< Write the acceleration x-axis value to the App.
	SET_GYRO_TIME		= 0x1B,
	SET_GYRO_LIMIT		= 0x1C,
	SET_ACCEL			= 0x1D,		//!< Write the acceleration x-axis value to the App.
	SET_ACCEL_TIME		= 0x1E,
	SET_ACCEL_LIMIT		= 0x1F,
	SET_MPU9250_INT		= 0x20,
	SET_MAG				= 0x21,
	SET_MAG_TIME		= 0x22,
	SET_MAG_LIMIT		= 0x23,

	GET_CONNECTION		= 0xF0,		//!< Get the BLE connection information.
	GET_DEBUG_MODE		= 0xF1,		//!< Get the debug mode enable or disable.
	GET_ERROR_STATUS_BYTE = 0xE2,
	GET_RESET			= 0xFF,		//!< Resets the MCU.
}BLE_CMD;

typedef void (*callback_t)(uint8_t connection);

void uart_init(void);

void uart_enableCallback(BLE_CMD cmd, callback_t fkt);

void uart_disableCallback(BLE_CMD cmd);

uint16_t uart_readValue(BLE_CMD cmd);

void uart_getGyroData(uint32_t *timestamp, uint32_t *data);
void uart_getAccelData(uint32_t *timestamp, uint32_t *data);
void uart_getMagnetData(uint32_t *timestamp, uint32_t *data);
void uart_getGyroLimit(uint16_t *data);
void uart_getAccelLimit(uint16_t *data);
void uart_getMagnetLimit(uint16_t *data);
void uart_clearMPU9250Interrupt(void);

int8_t uart_getSppData(void);

void uart_sendCmd(BLE_CMD cmd, uint16_t *data, uint8_t len);

int8_t uart_sendUpdate(char cmd, uint8_t *data, uint8_t len);

#endif /* INC_UART_H_ */
