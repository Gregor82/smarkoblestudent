/***************************************************************************//**
 * @brief app.h
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef APP_H_
#define APP_H_

#include "gecko_configuration.h"

/* DEBUG_LEVEL is used to enable/disable debug prints. Set DEBUG_LEVEL to 1 to enable debug prints */
#define DEBUG_LEVEL 0

/* Set this value to 1 if you want to disable deep sleep completely */
#define DISABLE_SLEEP 0

#if DEBUG_LEVEL
#include "retargetserial.h"
#include <stdio.h>
#endif

#if DEBUG_LEVEL
#define initLog()     RETARGET_SerialInit()
#define flushLog()    RETARGET_SerialFlush()
#define printLog(...) printf(__VA_ARGS__)
#else
#define initLog()
#define flushLog()
#define printLog(...)
#endif

/** GATT Server Attribute Value Write Configuration.
 *  Structure to register handler functions to characteristic write events. */
typedef struct {
  uint16_t charId;                      /**< ID of the Characteristic. */
  void (*fctn)(uint8_t connection, uint8array *writeValue); /**< Handler function. */
} AppBleGattServerAttributeValue_t;

/** GATT Server Attribute User Read Configuration.
 *  Structure to register handler functions to user read events. */
typedef struct {
  uint16_t charId;                      /**< ID of the Characteristic. */
  void (*fctn)(uint8_t connection);                   /**< Handler function. */
} AppBleGattServerUserReadRequest_t;

/** GATT Server Attribute  User Write Configuration.
 *  Structure to register handler functions to user write events. */
typedef struct {
  uint16_t charId;                      /**< ID of the Characteristic. */
  void (*fctn)(uint8_t connection, uint8array *writeValue); /**< Handler function. */
} AppBleGattServerUserWriteRequest_t;

/** GATT Server CCC Status Change Configuration.
 *  Structure to register handler functions to client characteristic
 *  configuration status change events. */
typedef struct {
  uint16_t charId;                                          /**< ID of the Characteristic. */
  void (*fctn)(uint8_t connection, uint16_t clientConfig);  /**< Handler function. */
} AppBleGattServerCharStatus_t;

/** GATT Server Confirmation.
 *  Structure to register handler functions to be called when a confirmation is received. */
typedef struct {
  uint16_t charId;                      /**< ID of the Characteristic. */
  void (*fctn)(uint8_t connection);     /**< Handler function. */
} AppBleGattServerConfirmation_t;

extern AppBleGattServerAttributeValue_t AppBleGattServerAttributeValue[];
extern AppBleGattServerUserReadRequest_t AppBleGattServerUserReadRequest[];
extern AppBleGattServerUserWriteRequest_t AppBleGattServerUserWriteRequest[];
extern AppBleGattServerConfirmation_t AppBleGattServerConfirmation[];
extern AppBleGattServerCharStatus_t AppBleGattServerCharStatus[];

/* Main application */
void appMain(gecko_configuration_t *pconfig);

#endif
