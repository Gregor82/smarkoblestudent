/***********************************************************************************************//**
 * \file   aio_service.c
 * \brief  Automation I/O Service
 ***************************************************************************************************
 * <b> (C) Copyright 2015 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

#include "aio_service.h"
/* standard library headers */
#include <stdbool.h>

/* BG stack headers */
#include "gatt_db.h"
#include "native_gecko.h"
#include "infrastructure.h"

#include "uart.h"


/***********************************************************************************************//**
 * @addtogroup Features
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup aio
 * @{
 **************************************************************************************************/

/***************************************************************************************************
 * Local Macros and Definitions
 **************************************************************************************************/

/** Indicates currently there is no active connection using this service. */
#define AIO_NO_CONNECTION               0xFF

/***************************************************************************************************
 * Local Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Local Variables
 **************************************************************************************************/

/***************************************************************************************************
 * Static Function Declarations
 **************************************************************************************************/
static bool digitalInNotification = false;

/***************************************************************************************************
 * Public Function Definitions
 **************************************************************************************************/
void aioServiceInit(void)
{

}

void aioServiceDeInit(void)
{

}

void aioServiceConnectionOpened(void)
{

}

void aioServiceConnectionClosed(void)
{
  digitalInNotification = false;
}

void aioServiceDigitalInCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
  /* If the new value of CCC is not 0 (either indication or notification enabled) */
  if (clientConfig) {
    digitalInNotification = true;
  } else {
    digitalInNotification = false;
  }
}

void aioServiceDigitalWriteReset(uint8_t connection, uint8array *writeValue)
{
	uint16_t data = 0x01;
	uart_sendCmd(GET_RESET, &data, 1);

	gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_aio_digital_reset, bg_err_success);
}

void aioServiceDigitalInRead(uint8_t connection)
{

}

/***************************************************************************************************
 * Static Function Definitions
 **************************************************************************************************/

/** @} (end addtogroup aio) */
/** @} (end addtogroup Features) */
