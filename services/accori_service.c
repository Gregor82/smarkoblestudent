/***********************************************************************************************//**
 * \file   accori_service.c
 * \brief  Acceleration and Orientation Service
 ***************************************************************************************************
 * <b> (C) Copyright 2015 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

#include "accori_service.h"
#include <stdbool.h>

/* BG stack headers */
#include "gatt_db.h"
#include "native_gecko.h"
#include "infrastructure.h"

/* framework specific headers */

/* profiles */
//#include "connection.h"

/* application specific headers */
#include "app_timer.h"
#include "uart.h"

/***********************************************************************************************//**
 * @addtogroup Features
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup accor
 * @{
 **************************************************************************************************/

/***************************************************************************************************
 * Local Macros and Definitions
 **************************************************************************************************/
// Number of axis for acceleration and orientation
#define ACC_AXIS                               3
#define ORI_AXIS                               3
#define MAG_AXIS                               3

// Acceleration and orientation axis payload length in bytes
#define ACC_AXIS_PAYLOAD_LENGTH                4
#define ORI_AXIS_PAYLOAD_LENGTH                4
#define MAG_AXIS_PAYLOAD_LENGTH                4

// Inertial payload length in bytes
#define ACCELERATION_PAYLOAD_LENGTH  (ACC_AXIS * ACC_AXIS_PAYLOAD_LENGTH + 4)
#define ORIENTATION_PAYLOAD_LENGTH   (ORI_AXIS * ORI_AXIS_PAYLOAD_LENGTH + 4)
#define MAGNET_PAYLOAD_LENGTH   	(MAG_AXIS * MAG_AXIS_PAYLOAD_LENGTH + 4)


/***************************************************************************************************
 * Local Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Local Variables
 **************************************************************************************************/
static bool accelerationNotification = false;
static bool orientationNotification  = false;
static bool magnetNotification  = false;
static bool interruptNotification  = false;

/***************************************************************************************************
 * Public Variable Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Static Function Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Public Function Definitions
 **************************************************************************************************/
void accoriServiceInit(void)
{
  accelerationNotification = false;
  orientationNotification  = false;
  magnetNotification  = false;
  interruptNotification  = false;
}

void accoriServiceDeInit(void)
{
	// Nothing to do
}

void accoriServiceConnectionOpened(void)
{
	// Nothing to do
}

void accoriServiceConnectionClosed(void)
{
  accelerationNotification = false;
  orientationNotification  = false;
  magnetNotification  = false;
  interruptNotification  = false;
}

void accoriServiceAccelerationCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
  accelerationNotification = (clientConfig > 0);
  if (accelerationNotification) {
	  accoriServiceAccelerationMeasurement(connection);
	  uart_enableCallback(SET_ACCEL, accoriServiceAccelerationMeasurement);
  } else {
	  uart_disableCallback(SET_GYRO);
  }
}

void accoriServiceOrientationCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
  orientationNotification = (clientConfig > 0);
  if (orientationNotification) {
	  accoriServiceOrientationMeasurement(connection);
	  uart_enableCallback(SET_GYRO, accoriServiceOrientationMeasurement);
  } else {
	  uart_disableCallback(SET_GYRO);
  }
}

void accoriServiceMagnetCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
	magnetNotification = (clientConfig > 0);
  if (magnetNotification) {
	  accoriServiceMagnetMeasurement(connection);
	  uart_enableCallback(SET_MAG, accoriServiceMagnetMeasurement);
  } else {
	  uart_disableCallback(SET_MAG);
  }
}

void accoriServiceMPUInterruptCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
  interruptNotification = (clientConfig > 0);
  if (interruptNotification) {
	  accoriServiceMPUInterruptMeasurement(connection);
	  uart_enableCallback(SET_MPU9250_INT, accoriServiceMPUInterruptMeasurement);
  } else {
	  uart_disableCallback(SET_MPU9250_INT);
  }
}

void accoriServiceAccelerationMeasurement(uint8_t connection)
{
	uint32_t acc[3];
	uint8_t buffer[ACCELERATION_PAYLOAD_LENGTH];
	uint8_t *p = buffer;
	uint32_t timestamp;

	if (!accelerationNotification) {
		return;
	}

	uart_getAccelData(&timestamp, acc);
	UINT32_TO_BITSTREAM(p, timestamp);
	UINT32_TO_BITSTREAM(p, acc[0]);
	UINT32_TO_BITSTREAM(p, acc[1]);
	UINT32_TO_BITSTREAM(p, acc[2]);

	gecko_cmd_gatt_server_send_characteristic_notification(connection, gattdb_accor_acceleration, ACCELERATION_PAYLOAD_LENGTH, buffer);
}

void accoriServiceOrientationMeasurement(uint8_t connection)
{
	uint32_t gyro[3];
	uint8_t buffer[ORIENTATION_PAYLOAD_LENGTH];
	uint8_t *p = buffer;
	uint32_t timestamp;

	if (!orientationNotification) {
		return;
	}

	uart_getGyroData(&timestamp, gyro);
	UINT32_TO_BITSTREAM(p, timestamp);
	UINT32_TO_BITSTREAM(p, gyro[0]);
	UINT32_TO_BITSTREAM(p, gyro[1]);
	UINT32_TO_BITSTREAM(p, gyro[2]);

	gecko_cmd_gatt_server_send_characteristic_notification(connection, gattdb_accor_orientation, ORIENTATION_PAYLOAD_LENGTH, buffer);
}

void accoriServiceMagnetMeasurement(uint8_t connection)
{
	uint32_t mag[3];
	uint8_t buffer[MAGNET_PAYLOAD_LENGTH];
	uint8_t *p = buffer;
	uint32_t timestamp;

	if (!magnetNotification) {
		return;
	}

	uart_getMagnetData(&timestamp, mag);
	UINT32_TO_BITSTREAM(p, timestamp);
	UINT32_TO_BITSTREAM(p, mag[0]);
	UINT32_TO_BITSTREAM(p, mag[1]);
	UINT32_TO_BITSTREAM(p, mag[2]);

	gecko_cmd_gatt_server_send_characteristic_notification(connection, gattdb_accor_magnet, MAGNET_PAYLOAD_LENGTH, buffer);
}

void accoriServiceMPUInterruptMeasurement(uint8_t connection)
{
	uint16_t interruptMPU;
	uint8_t buffer[3];
	uint8_t *p = buffer;

	if (!interruptNotification) {
		return;
	}

	/* Update battery level based on battery level sensor */
	interruptMPU = uart_readValue(SET_MPU9250_INT);
	UINT16_TO_BITSTREAM(p, (uint16_t)interruptMPU);

	gecko_cmd_gatt_server_send_characteristic_notification(connection, gattdb_accor_interrupt, 3, buffer);
}

void accoriServiceAccelerationRead(uint8_t connection)
{
	uint32_t acc[3];
	uint8_t buffer[ACCELERATION_PAYLOAD_LENGTH];
	uint8_t *p = buffer;
	uint32_t timestamp;

	uart_getAccelData(&timestamp, acc);
	UINT32_TO_BITSTREAM(p, timestamp);
	UINT32_TO_BITSTREAM(p, acc[0]);
	UINT32_TO_BITSTREAM(p, acc[1]);
	UINT32_TO_BITSTREAM(p, acc[2]);

	/* Send response to read request */
	gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_accor_acceleration, 0, ACCELERATION_PAYLOAD_LENGTH, buffer);
}

void accoriServiceOrientationRead(uint8_t connection)
{
	uint32_t gyro[3];
	uint8_t buffer[ORIENTATION_PAYLOAD_LENGTH];
	uint8_t *p = buffer;
	uint32_t timestamp;

	uart_getGyroData(&timestamp, gyro);
	UINT32_TO_BITSTREAM(p, timestamp);
	UINT32_TO_BITSTREAM(p, gyro[0]);
	UINT32_TO_BITSTREAM(p, gyro[1]);
	UINT32_TO_BITSTREAM(p, gyro[2]);

	/* Send response to read request */
	gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_accor_orientation, 0, ORIENTATION_PAYLOAD_LENGTH, buffer);
}

void accoriServiceMagnetRead(uint8_t connection)
{
	uint32_t mag[3];
	uint8_t buffer[MAGNET_PAYLOAD_LENGTH];
	uint8_t *p = buffer;
	uint32_t timestamp;

	uart_getMagnetData(&timestamp, mag);
	UINT32_TO_BITSTREAM(p, timestamp);
	UINT32_TO_BITSTREAM(p, mag[0]);
	UINT32_TO_BITSTREAM(p, mag[1]);
	UINT32_TO_BITSTREAM(p, mag[2]);

	/* Send response to read request */
	gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_accor_magnet, 0, MAGNET_PAYLOAD_LENGTH, buffer);
}

void accoriServiceAccelLimitRead(uint8_t connection)
{
	uint16_t accLimt[3];
	uint8_t buffer[ACCELERATION_PAYLOAD_LENGTH];
	uint8_t *p = buffer;

	uart_getAccelLimit(accLimt);
	UINT16_TO_BITSTREAM(p, accLimt[0]);
	UINT16_TO_BITSTREAM(p, accLimt[1]);
	UINT16_TO_BITSTREAM(p, accLimt[2]);

	/* Send response to read request */
	gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_accor_accelLimit, 0, ACCELERATION_PAYLOAD_LENGTH, buffer);
}

void accoriServiceOrientLimitRead(uint8_t connection)
{
	uint16_t gyroLimit[3];
	uint8_t buffer[ORIENTATION_PAYLOAD_LENGTH];
	uint8_t *p = buffer;

	uart_getGyroLimit(gyroLimit);
	UINT16_TO_BITSTREAM(p, gyroLimit[0]);
	UINT16_TO_BITSTREAM(p, gyroLimit[1]);
	UINT16_TO_BITSTREAM(p, gyroLimit[2]);

	/* Send response to read request */
	gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_accor_orientLimit, 0, ORIENTATION_PAYLOAD_LENGTH, buffer);
}

void accoriServiceMagnetLimitRead(uint8_t connection)
{
	uint16_t magLimit[3];
	uint8_t buffer[MAGNET_PAYLOAD_LENGTH];
	uint8_t *p = buffer;

	uart_getMagnetLimit(magLimit);
	UINT16_TO_BITSTREAM(p, magLimit[0]);
	UINT16_TO_BITSTREAM(p, magLimit[1]);
	UINT16_TO_BITSTREAM(p, magLimit[2]);

	/* Send response to read request */
	gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_accor_magnetLimit, 0, MAGNET_PAYLOAD_LENGTH, buffer);
}

void accoriServiceTimeRead(uint8_t connection)
{
	uint16_t gyroTime;
	uint16_t accelTime;
	uint16_t magTime;
	uint8_t buffer[6];
	uint8_t *p = buffer;

	/* Update battery level based on battery level sensor */
	gyroTime = uart_readValue(SET_GYRO_TIME);
	accelTime = uart_readValue(SET_ACCEL_TIME);
	magTime = uart_readValue(SET_MAG_TIME);
	UINT16_TO_BITSTREAM(p, (uint16_t)gyroTime);
	UINT16_TO_BITSTREAM(p, (uint16_t)accelTime);
	UINT16_TO_BITSTREAM(p, (uint16_t)magTime);

	/* Send response to read request */
	gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_accor_time, 0, 6, buffer);
}

void accoriServiceMPUInterruptRead(uint8_t connection)
{
	uint16_t interruptMPU;
	uint8_t buffer[3];
	uint8_t *p = buffer;

	interruptMPU = uart_readValue(SET_MPU9250_INT);
	UINT16_TO_BITSTREAM(p, (uint16_t)interruptMPU);

	/* Send response to read request */
	gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_accor_interrupt, 0, 3, buffer);
}

void accoriServiceAccelLimitWrite(uint8_t connection, uint8array *writeValue)
{
	uint16_t value[3];
	value[0] = (writeValue->data[1] << 8) | writeValue->data[0];
	value[1] = (writeValue->data[3] << 8) | writeValue->data[2];
	value[2] = (writeValue->data[5] << 8) | writeValue->data[4];

	uart_sendCmd(SET_ACCEL_LIMIT, value, 3);
	gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_accor_accelLimit, bg_err_success);
}

void accoriServiceOrientLimitWrite(uint8_t connection, uint8array *writeValue)
{
	uint16_t value[3];
	value[0] = (writeValue->data[1] << 8) | writeValue->data[0];
	value[1] = (writeValue->data[3] << 8) | writeValue->data[2];
	value[2] = (writeValue->data[5] << 8) | writeValue->data[4];

	uart_sendCmd(SET_GYRO_LIMIT, value, 3);
	gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_accor_orientLimit, bg_err_success);
}

void accoriServiceMagnetLimitWrite(uint8_t connection, uint8array *writeValue)
{
	uint16_t value[3];
	value[0] = (writeValue->data[1] << 8) | writeValue->data[0];
	value[1] = (writeValue->data[3] << 8) | writeValue->data[2];
	value[2] = (writeValue->data[5] << 8) | writeValue->data[4];

	uart_sendCmd(SET_MAG_LIMIT, value, 3);
	gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_accor_magnetLimit, bg_err_success);
}

void accoriServiceTimeWrite(uint8_t connection, uint8array *writeValue)
{
	uint16_t value = (writeValue->data[1] << 8) | writeValue->data[0];
	uart_sendCmd(SET_ACCEL_TIME, &value, 1);

	value = (writeValue->data[3] << 8) | writeValue->data[2];
	uart_sendCmd(SET_GYRO_TIME, &value, 1);

	gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_accor_time, bg_err_success);
}

void accoriServiceMPUInterruptWrite(uint8_t connection, uint8array *writeValue)
{
	uart_clearMPU9250Interrupt();
	gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_accor_interrupt, bg_err_success);

}

/** @} (end addtogroup accor) */
/** @} (end addtogroup Features) */
