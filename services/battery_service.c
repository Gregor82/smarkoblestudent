/***********************************************************************************************//**
 * \file   battery_service.c
 * \brief  Battery Service
 ***************************************************************************************************
 * <b> (C) Copyright 2015 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

/* standard library headers */
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

/* BG stack headers */
#include "bg_types.h"
#include "gatt_db.h"
#include "native_gecko.h"
#include "infrastructure.h"



/* Own header*/
#include "uart.h"
#include "battery_service.h"

/***********************************************************************************************//**
 * @addtogroup Features
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup battery
 * @{
 **************************************************************************************************/

/***************************************************************************************************
 * Local Macros and Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Local Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Local Variables
 **************************************************************************************************/
static bool batteryLevelNotification  = false;
static bool batteryVoltageNotification = false;
static bool batteryCurrentNotification = false;
static bool batteryTemperatureNotification  = false;

static uint16_t batteryLevel; 			/* Battery Level */
static uint16_t batteryVoltage; 		/* Battery Voltage */
static uint16_t batteryCurrent; 		/* Battery Current */
static uint16_t batteryTemperature; 	/* Battery Temperature */


/***************************************************************************************************
 * Static Function Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Public Variable Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Public Function Definitions
 **************************************************************************************************/
void batteryServiceInit(void)
{
	batteryLevelNotification  = false;
	batteryVoltageNotification = false;
	batteryCurrentNotification = false;
	batteryTemperatureNotification  = false;
}

void batteryServiceDeInit(void)
{
	// Nothing to do
}

void batteryServiceConnectionOpened(void)
{
	// Nothing to do
}

void batteryServiceConnectionClosed(void)
{
	batteryLevelNotification  = false;
	batteryVoltageNotification = false;
	batteryCurrentNotification = false;
	batteryTemperatureNotification  = false;
}

void batteryServiceLeveCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
	batteryLevelNotification = (clientConfig > 0);
	if (batteryLevelNotification) {
		batteryLevelServiceMeasure(connection);
		uart_enableCallback(SET_BATTERY_SOC, batteryLevelServiceMeasure);
	}
	else
		uart_disableCallback(SET_BATTERY_SOC);
}

void batteryServiceVoltagCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
	batteryVoltageNotification = (clientConfig > 0);
	if (batteryVoltageNotification) {
		batteryVoltageServiceMeasure(connection);
		uart_enableCallback(SET_BATTERY_VOLTAGE, batteryVoltageServiceMeasure);
	}
	else
		uart_disableCallback(SET_BATTERY_VOLTAGE);
}

void batteryServiceCurrentCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
	batteryCurrentNotification = (clientConfig > 0);
	if (batteryCurrentNotification) {
		batteryCurrentServiceMeasure(connection);
		uart_enableCallback(SET_BATTERY_CURRENT, batteryCurrentServiceMeasure);
	}
	else
		uart_disableCallback(SET_BATTERY_CURRENT);
}

void batteryServiceTemperatureCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
	batteryTemperatureNotification = (clientConfig > 0);
	if (batteryTemperatureNotification) {
		batteryTemperatureServiceMeasure(connection);
		uart_enableCallback(SET_BATTERY_TEMP, batteryCurrentServiceMeasure);
	}
	else
		uart_disableCallback(SET_BATTERY_TEMP);
}

void batteryLevelServiceMeasure(uint8_t connection)
{
	uint8_t buffer[3];
	uint8_t *p = buffer;

	if (!batteryLevelNotification) {
		return;
	}

	/* Update battery level based on battery level sensor */
	batteryLevel = uart_readValue(SET_BATTERY_SOC);
	UINT16_TO_BITSTREAM(p, (uint16_t)batteryLevel);

	gecko_cmd_gatt_server_send_characteristic_notification(connection, gattdb_battery_level, 3, buffer);
}

void batteryVoltageServiceMeasure(uint8_t connection)
{
	uint8_t buffer[3];
	uint8_t *p = buffer;

	if (!batteryVoltageNotification) {
		return;
	}

	/* Update battery level based on battery level sensor */
	batteryVoltage = uart_readValue(SET_BATTERY_VOLTAGE);
	UINT16_TO_BITSTREAM(p, (uint16_t)batteryVoltage)

	gecko_cmd_gatt_server_send_characteristic_notification(connection, gattdb_battery_voltage, 3, buffer);
}

void batteryCurrentServiceMeasure(uint8_t connection)
{
	uint8_t buffer[3];
	uint8_t *p = buffer;

	if (!batteryCurrentNotification) {
		return;
	}

	/* Update battery level based on battery level sensor */
	batteryCurrent = uart_readValue(SET_BATTERY_CURRENT);
	UINT16_TO_BITSTREAM(p, (uint16_t)batteryCurrent);

	gecko_cmd_gatt_server_send_characteristic_notification(connection, gattdb_battery_current, 3, buffer);
}

void batteryTemperatureServiceMeasure(uint8_t connection)
{
	uint8_t buffer[3];
	uint8_t *p = buffer;

	if (!batteryTemperatureNotification) {
		return;
	}

	/* Update battery level based on battery level sensor */
	batteryTemperature = uart_readValue(SET_BATTERY_TEMP);
	UINT16_TO_BITSTREAM(p, (uint16_t)batteryTemperature);

	gecko_cmd_gatt_server_send_characteristic_notification(connection, gattdb_battery_temperature, 3, buffer);
}

void batteryServiceLevelRead(uint8_t connection)
{
  uint8_t buffer[2];
  uint8_t *p = buffer;

  /* Update battery level based on battery level sensor */
  batteryLevel = uart_readValue(SET_BATTERY_SOC);
  UINT16_TO_BITSTREAM(p, (uint16_t)batteryLevel);

  /* Send response to read request */
  gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_battery_level, 0, 2, buffer);
}

void batteryServiceVoltageRead(uint8_t connection)
{
  uint8_t buffer[2];
  uint8_t *p = buffer;

  /* Update battery level based on battery level sensor */
  batteryVoltage = uart_readValue(SET_BATTERY_VOLTAGE);
  UINT16_TO_BITSTREAM(p, (uint16_t)batteryVoltage);

  /* Send response to read request */
  gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_battery_voltage, 0, 2, buffer);
}

void batteryServiceCurrentRead(uint8_t connection)
{
  uint8_t buffer[2];
  uint8_t *p = buffer;

  /* Update battery level based on battery level sensor */
  batteryCurrent = uart_readValue(SET_BATTERY_CURRENT);
  UINT16_TO_BITSTREAM(p, (uint16_t)batteryCurrent);

  /* Send response to read request */
  gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_battery_current, 0, 2, buffer);
}

void batteryServiceTemperatureRead(uint8_t connection)
{
  uint8_t buffer[2];
  uint8_t *p = buffer;

  /* Update battery level based on battery level sensor */
  batteryTemperature = uart_readValue(SET_BATTERY_TEMP);
  UINT16_TO_BITSTREAM(p, (uint16_t)batteryTemperature);

  /* Send response to read request */
  gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_battery_temperature, 0, 2, buffer);
}

/** @} (end addtogroup battery) */
/** @} (end addtogroup Features) */
