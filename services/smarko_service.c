/***********************************************************************************************//**
 * \file   battery_service.c
 * \brief  Battery Service
 ***************************************************************************************************
 * <b> (C) Copyright 2015 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

/* standard library headers */
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

/* BG stack headers */
#include "bg_types.h"
#include "gatt_db.h"
#include "native_gecko.h"
#include "infrastructure.h"



/* Own header*/
#include "smarko_service.h"
#include "uart.h"

/***********************************************************************************************//**
 * @addtogroup Features
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup battery
 * @{
 **************************************************************************************************/

/***************************************************************************************************
 * Local Macros and Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Local Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Local Variables
 **************************************************************************************************/
static bool errorStatusNotification  = false;

static uint16_t errorStatus;

/***************************************************************************************************
 * Static Function Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Public Variable Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Public Function Definitions
 **************************************************************************************************/
void smarkoServiceInit(void)
{
	errorStatusNotification  = false;
}


void smarkoServiceConnectionOpened(void)
{

}

void smarkoServiceConnectionClosed(void)
{
	errorStatusNotification  = false;
}

void smarkoStatusServiceCharStatusChange(uint8_t connection, uint16_t clientConfig)
{
	errorStatusNotification = (clientConfig > 0);
	if (errorStatusNotification) {
		smarkoStatusServiceMeasure(connection);
		uart_enableCallback(GET_ERROR_STATUS_BYTE, smarkoStatusServiceMeasure);
	}
	else
		uart_disableCallback(GET_ERROR_STATUS_BYTE);
}

void smarkoStatusServiceMeasure(uint8_t connection)
{
	uint8_t buffer[15];
	uint8_t *p = buffer;

	if (!errorStatusNotification) {
		return;
	}

	/* Update battery level based on battery level sensor */
	errorStatus = uart_readValue(GET_ERROR_STATUS_BYTE);
	UINT16_TO_BITSTREAM(p, (uint16_t)errorStatus);

	gecko_cmd_gatt_server_send_characteristic_notification(connection, gattdb_smarko_status, 3, buffer);
}

void smarkoStatusRead(uint8_t connection)
{
	  uint8_t buffer[2];
	  uint8_t *p = buffer;

	  /* Update battery level based on battery level sensor */
	  errorStatus = uart_readValue(GET_ERROR_STATUS_BYTE);
	  UINT16_TO_BITSTREAM(p, (uint16_t)errorStatus);

	  /* Send response to read request */
	  gecko_cmd_gatt_server_send_user_read_response(connection, gattdb_smarko_status, 0, 2, buffer);
}

void smarkoServiceSendInit(uint8_t connection, uint8array *writeValue)
{
	int8_t ret = uart_sendUpdate('S', &writeValue->data[0], 1);

	gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_smarko_init, ret);
}

void smarkoServiceUpdate(uint8_t connection, uint8array *writeValue)
{
	if(uart_sendUpdate('U', writeValue->data, writeValue->len))
		gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_smarko_update, bg_err_success);
	else
		gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_smarko_update, (int8_t)bg_err_hardware);
}

void smarkoServiceFinish(uint8_t connection, uint8array *writeValue)
{
	if(uart_sendUpdate('F', writeValue->data, writeValue->len))
		gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_smarko_finish, bg_err_success);
	else
		gecko_cmd_gatt_server_send_user_write_response(connection, gattdb_smarko_finish, (int8_t)bg_err_hardware);
}

/** @} (end addtogroup battery) */
/** @} (end addtogroup Features) */
