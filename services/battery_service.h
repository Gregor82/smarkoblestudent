/***********************************************************************************************//**
 * \file   battery_service.h
 * \brief  Battery Service
 ***************************************************************************************************
 * <b> (C) Copyright 2015 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

#ifndef BATTERY_SERVICE_H
#define BATTERY_SERVICE_H

#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************************************//**
 * \defgroup battery Battery
 * \brief Battery Service API
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup Features
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup battery
 * @{
 **************************************************************************************************/

/***************************************************************************************************
 * Public Macros and Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Function Declarations
 **************************************************************************************************/

/***********************************************************************************************//**
 *  \brief  Initialise Battery Service.
 *  \details  Initialise the connection ID, the configuration flags of the temperature measurement
 *  and stop temperature measurement timer.
 **************************************************************************************************/
void batteryServiceInit(void);
void batteryServiceDeInit(void);
void batteryServiceConnectionOpened(void);
void batteryServiceConnectionClosed(void);

/***********************************************************************************************//**
 *  \brief  Battery CCCD has changed event handler function.
 *  \param[in]  connection  Connection ID.
 *  \param[in]  clientConfig  New value of CCCD.
 **************************************************************************************************/
void batteryServiceLeveCharStatusChange(uint8_t connection, uint16_t clientConfig);
void batteryServiceVoltagCharStatusChange(uint8_t connection, uint16_t clientConfig);
void batteryServiceCurrentCharStatusChange(uint8_t connection, uint16_t clientConfig);
void batteryServiceTemperatureCharStatusChange(uint8_t connection, uint16_t clientConfig);

/***********************************************************************************************//**
 *  \brief  Make one battery measurement.
 **************************************************************************************************/
void batteryServiceMeasure(uint8_t connection);
void batteryLevelServiceMeasure(uint8_t connection);
void batteryVoltageServiceMeasure(uint8_t connection);
void batteryCurrentServiceMeasure(uint8_t connection);
void batteryTemperatureServiceMeasure(uint8_t connection);

/***********************************************************************************************//**
 *  \brief  Read battery measurement.
 **************************************************************************************************/
void batteryServiceLevelRead(uint8_t connection);
void batteryServiceVoltageRead(uint8_t connection);
void batteryServiceCurrentRead(uint8_t connection);
void batteryServiceTemperatureRead(uint8_t connection);

/** @} (end addtogroup hr) */
/** @} (end addtogroup Features) */

#ifdef __cplusplus
};
#endif

#endif /* BATTERY_SERVICE_H */
