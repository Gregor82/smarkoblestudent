/***********************************************************************************************//**
 * \file   battery_service.h
 * \brief  Battery Service
 ***************************************************************************************************
 * <b> (C) Copyright 2015 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

#ifndef SMARKO_SERVICE_H
#define SMARKO_SERVICE_H

#include <stdint.h>
#include "bg_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************************************//**
 * \defgroup SPP
 * \brief SPP Service API
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup Features
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup SPP
 * @{
 **************************************************************************************************/

/***************************************************************************************************
 * Public Macros and Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Function Declarations
 **************************************************************************************************/

/***********************************************************************************************//**
 *  \brief  Initialise SPP Service.
 *  \details
 **************************************************************************************************/
void smarkoServiceInit(void);

void smarkoServiceConnectionOpened(void);

/**********************************************************************************************//**
 * @brief
 *   Must be called whenever the connection is closed.
 * @return
 *   None
 *************************************************************************************************/
void smarkoServiceConnectionClosed(void);

/***********************************************************************************************//**
 *  \brief  SPP CCCD has changed event handler function.
 *  \param[in]  connection  Connection ID.
 *  \param[in]  clientConfig  New value of CCCD.
 **************************************************************************************************/
void smarkoStatusServiceCharStatusChange(uint8_t connection, uint16_t clientConfig);

void smarkoStatusServiceMeasure(uint8_t connection);
void smarkoStatusRead(uint8_t connection);

/***********************************************************************************************//**
 *  \brief  Write data.
 **************************************************************************************************/
void smarkoServiceSendInit(uint8_t connection, uint8array *writeValue);
void smarkoServiceUpdate(uint8_t connection, uint8array *writeValue);
void smarkoServiceFinish(uint8_t connection, uint8array *writeValue);

/***********************************************************************************************//**
 *  \brief  .
 **************************************************************************************************/
//void batteryServiceLevelRead(void);

/** @} (end addtogroup hr) */
/** @} (end addtogroup Features) */

#ifdef __cplusplus
};
#endif

#endif /* SMARKO_SERVICE_H */
