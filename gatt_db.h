// Copyright 2019 Silicon Laboratories, Inc.
//
//

/********************************************************************
 * Autogenerated file, do not edit.
 *******************************************************************/

#ifndef __GATT_DB_H
#define __GATT_DB_H

#include "bg_gattdb_def.h"

extern const struct bg_gattdb_def bg_gattdb_data;

#define gattdb_service_changed_char             3
#define gattdb_database_hash                    6
#define gattdb_client_support_features          8
#define gattdb_device_name                     11
#define gattdb_system_id                       20
#define gattdb_battery_level                   23
#define gattdb_battery_voltage                 26
#define gattdb_battery_current                 29
#define gattdb_battery_temperature             32
#define gattdb_aio_digital_in                  36
#define gattdb_aio_digital_reset               39
#define gattdb_accor_acceleration              42
#define gattdb_accor_orientation               45
#define gattdb_accor_magnet                    48
#define gattdb_accor_cp                        51
#define gattdb_accor_accelLimit                54
#define gattdb_accor_orientLimit               56
#define gattdb_accor_magnetLimit               58
#define gattdb_accor_time                      60
#define gattdb_accor_interrupt                 62
#define gattdb_smarko_init                     66
#define gattdb_smarko_finish                   68
#define gattdb_smarko_update                   70
#define gattdb_smarko_status                   72
#define gattdb_ota_control                     76

#endif
