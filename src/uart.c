/**
 * @file			uart.cstatic
 * @date			16.11.2017
 *	
 * @details
 */
/******************** include files ***********************/
#include <string.h>
#include <stdlib.h>
#include "ustimer.h"
#include "uartdrv.h"
#include "uart.h"
#include "native_gecko.h"

/******************** defines *****************************/
#define RX_BUFFERSIZE 			32
#define TX_BUFFERSIZE 			255

/******************** prototypes **************************/
static void uart_transmit(uint8_t *data, uint8_t dataLen);
static void uart_checkCmd(uint8_t *buf, uint16_t offset);
static void uart_rxHandler(uint16_t offset);

static void uart_txCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount);
static void uart_rxCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount);

/******************** typedef *****************************/
/**
 * struct for buffer
 */
typedef struct{
	/*@{*/
	uint8_t write;		/**< write */
	uint8_t read;		/**< read */
	uint8_t size;		/**< size */
	bool overflow;		/**< overflow */
	struct{
		uint8_t bufLen;
		uint8_t buffer[TX_BUFFERSIZE];	/**< buffer */
	//}*data;
	} data[EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS];
	/*@}*/
}UARTBuffer_t;

typedef struct {
	int16_t answer;
	uint32_t chunk;
	bool answered;
	bool running;
} Update_t;

typedef struct{
	uint8_t errorByte;
	uint8_t statusByte;
	uint16_t battery_soc;
	uint16_t battery_voltage;
	int16_t battery_current;
	uint16_t battery_temperature;

	uint16_t pulse;
	bool pulseIsNew;
	bool spo2IsNew;
	bool skinIsNew;
	uint16_t spo2;
	bool hasSkinContact;

	int16_t skin_temp;
	int16_t env_temp;
	int16_t env_humidiy;

	uint32_t gyroTimestamp_1;
	uint32_t gyro[3];
	uint32_t accelTimestamp_1;
	uint32_t accel[3];
	uint32_t magTimestamp_1;
	uint32_t mag[3];
	int16_t gyroLimit[3];
	int16_t accelLimit[3];
	int16_t magLimit[3];
	uint16_t gyroTime;
	uint16_t accelTime;
	uint16_t magTime;
	int16_t mpu9250Interrupt;

	bool connected;
	bool debugMode;
}RecvData_t;

/******************** struct data type ********************/
static UARTBuffer_t txBuffer = {
		.write		= 0,
		.read		= 0,
		.overflow 	= false,
		.size		= EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS,
};

static Update_t update = {
		.answer		= 0,
		.chunk 		= 0,
		.running	= false,
		.answered	= false,
};

static RecvData_t rcvData ={
		.errorByte = 0,
		.statusByte = 0,
		.battery_soc = 0,
		.battery_voltage = 0,
		.battery_current = 0,
		.battery_temperature = 0,
		.pulse = 0,
		.pulseIsNew = 0,
		.spo2IsNew = 0,
		.skinIsNew = 0,
		.spo2 = 0,
		.hasSkinContact = 0,
		.skin_temp = 0,
		.env_temp = 0,
		.env_humidiy = 0,
		.gyroTime = 0,
		.gyro = {0, 0, 0},
		.accelTime = 0,
		.accel = {0, 0, 0},
		.magTime = 0,
		.mag = {0, 0, 0},
		.gyroLimit = {0, 0, 0},
		.accelLimit = {0, 0, 0},
		.magLimit = {0, 0, 0},
		.gyroTime = 500,
		.accelTime = 500,
		.magTime = 500,
		.mpu9250Interrupt = 0,
		.connected = false,
		.debugMode = false,
};
/******************** private variables********************/
callback_t callback_batterySOC = NULL;
callback_t callback_batteryVoltage = NULL;
callback_t callback_batteryCurrent = NULL;
callback_t callback_batteryTemperature = NULL;
callback_t callback_hrmData = NULL;
callback_t callback_spo2Data = NULL;
callback_t callback_skinContact = NULL;
callback_t callback_bodyTemperature = NULL;
callback_t callback_environmentTemperatrue = NULL;
callback_t callback_environmentHumidity = NULL;
callback_t callback_errorStatus = NULL;
callback_t callback_accel = NULL;
callback_t callback_gyro = NULL;
callback_t callback_mag = NULL;
callback_t callback_mpu9250Int = NULL;

static UARTDRV_HandleData_t bleHandleData; /* UART driver handle */
static UARTDRV_Handle_t  bleHandle = &bleHandleData;

static uint8_t rxByte;
static uint32_t numOfChunks = 0;
static uint8_t rxBuf[RX_BUFFERSIZE];
static uint16_t rxBufCounter = 0;

/******************** private function ********************/
void uart_transmit(uint8_t *data, uint8_t dataLen)
{
	if((txBuffer.overflow) && (txBuffer.write == txBuffer.read))
		return;

	txBuffer.data[txBuffer.write].bufLen = dataLen;
	memcpy(txBuffer.data[txBuffer.write].buffer, data, dataLen*sizeof(uint8_t)+1);

	UARTDRV_Transmit(bleHandle, txBuffer.data[txBuffer.write].buffer, txBuffer.data[txBuffer.write].bufLen, uart_txCallback);

	txBuffer.write++;
	if(txBuffer.write >= txBuffer.size)
	{
		txBuffer.write = 0;
		txBuffer.overflow = true;
	}
}

void uart_checkCmd(uint8_t *buf, uint16_t offset)
{
	switch(buf[offset + 5])
	{
		case SET_BATTERY_SOC:
			rcvData.battery_soc = (buf[offset + 6] << 8) | buf[offset + 7];
			if(callback_batterySOC != NULL)
				callback_batterySOC(0xFF);
			break;
		case SET_BATTERY_VOLTAGE:
			rcvData.battery_voltage	= (buf[offset + 6] << 8) | buf[offset + 7];
			if(callback_batteryVoltage != NULL)
				callback_batteryVoltage(0xFF);
			break;
		case SET_BATTERY_CURRENT:
			rcvData.battery_current	= (buf[offset + 6] << 8) | buf[offset + 7];
			if(callback_batteryCurrent != NULL)
				callback_batteryCurrent(0xFF);
			break;
		case SET_BATTERY_TEMP:
			rcvData.battery_temperature = (buf[offset + 6] << 8) | buf[offset + 7];
			if(callback_batteryTemperature != NULL)
				callback_batteryTemperature(0xFF);
			break;
		case SET_PULSE_DATA:
			rcvData.pulse = (buf[offset + 6] << 8) | buf[offset + 7];
			rcvData.pulseIsNew = true;
			if(callback_hrmData != NULL)
				callback_hrmData(0xFF);
			break;
		case SET_SPO2_DATA:
			rcvData.spo2 = (buf[offset + 6] << 8) | buf[offset + 7];
			rcvData.spo2IsNew = true;
			if(callback_spo2Data != NULL)
				callback_spo2Data(0xFF);
			break;
		case SET_HAS_SKIN_CONTACT:
			rcvData.hasSkinContact = (buf[offset + 6] << 8) | buf[offset + 7];
			rcvData.skinIsNew = true;
			if(callback_skinContact != NULL)
				callback_skinContact(0xFF);
			break;
		case SET_BODY_TEMP:
			rcvData.skin_temp = (buf[offset + 6] << 8) | buf[offset + 7];
			if(callback_bodyTemperature != NULL)
				callback_bodyTemperature(0xFF);
			break;
		case SET_ENV_TEMP:
			rcvData.env_temp = (buf[offset + 6] << 8) | buf[offset + 7];
			if(callback_environmentTemperatrue != NULL)
				callback_environmentTemperatrue(0xFF);
			break;
		case SET_ENV_HUMIDITY:
			rcvData.env_humidiy = (buf[offset + 6] << 8) | buf[offset + 7];
			if(callback_environmentHumidity != NULL)
				callback_environmentHumidity(0xFF);
			break;
		case SET_GYRO:
			rcvData.gyroTimestamp_1 = (buf[offset + 6] << 24) | (buf[offset + 7] << 16) | (buf[offset + 8] << 8) | buf[offset + 9];
			for(uint8_t i = 0; i < 3; i++)
				//rcvData.gyro[i] =  = (buf[offset + 10 + 2 * i] << 8) | buf[offset + 11 + 2 * i];
				rcvData.gyro[i] = (buf[offset + 10 + 4 * i] << 24) | (buf[offset + 11 + 4 * i] << 16) | (buf[offset + 12 + 4 * i] << 8) | buf[offset + 13 + 4 * i];
			if(callback_gyro != NULL)
				callback_gyro(0xFF);
			break;
		case SET_ACCEL:
			rcvData.accelTimestamp_1 = (buf[offset + 6] << 24) | (buf[offset + 7] << 16) | (buf[offset + 8] << 8) | buf[offset + 9];
			for(uint8_t i = 0; i < 3; i++)
				//rcvData.accel[i] = (buf[offset + 10 + 2 * i] << 8) | buf[offset + 11 + 2 * i];
				rcvData.accel[i] = (buf[offset + 10 + 4 * i] << 24) | (buf[offset + 11 + 4 * i] << 16) | (buf[offset + 12 + 4 * i] << 8) | buf[offset + 13 + 4 * i];
			if(callback_accel != NULL)
				callback_accel(0xFF);
			break;
		case SET_MAG:
			rcvData.magTimestamp_1 = (buf[offset + 6] << 24) | (buf[offset + 7] << 16) | (buf[offset + 8] << 8) | buf[offset + 9];
			for(uint8_t i = 0; i < 3; i++)
				//rcvData.mag[i] = (buf[offset + 10 + 2 * i] << 8) | buf[offset + 11 + 2 * i];
				rcvData.mag[i] = (buf[offset + 10 + 4 * i] << 24) | (buf[offset + 11 + 4 * i] << 16) | (buf[offset + 12 + 4 * i] << 8) | buf[offset + 13 + 4 * i];
			if(callback_mag != NULL)
				callback_mag(0xFF);
			break;
		case SET_MPU9250_INT:
			rcvData.mpu9250Interrupt = (buf[offset + 6] << 8) | buf[offset + 7];
			if(callback_mpu9250Int != NULL)
				callback_mpu9250Int(0xFF);
			break;

		case GET_ERROR_STATUS_BYTE:
			rcvData.errorByte = buf[offset + 6];
			rcvData.statusByte = buf[offset + 7];
			if(callback_errorStatus!= NULL)
				callback_errorStatus(0xFF);
			break;
		case GET_RESET:
			break;
		case GET_CONNECTION:
			uart_sendCmd(GET_CONNECTION, (uint16_t *)rcvData.connected, 1);
			break;
		case GET_DEBUG_MODE:
			uart_sendCmd(GET_DEBUG_MODE, (uint16_t *)rcvData.debugMode, 1);
			break;
	}
}

void uart_rxHandler(uint16_t offset)
{
	switch(rxBuf[offset + 3])
	{
		case 'C':
			uart_checkCmd(rxBuf, offset);
			break;
		case 'U':
			update.answered	= true;

			switch (rxBuf[offset + 5])
			{
				case 'F':
					update.chunk = 0;
					update.answer = (rxBuf[offset + 6] << 8) || rxBuf[offset + 7];
					update.answered	= true;
					break;
				case 'U':

					update.chunk = (rxBuf[offset + 6] << 8) || rxBuf[offset + 7];
					update.answer = 3;
					update.answered	= true;
					break;
				case 'S':
					update.chunk = 0;
					update.answer = (rxBuf[offset + 6] << 8) || rxBuf[offset + 7];
					update.answered	= true;
					break;
			}
			break;
	}

}

/******************** callback function *******************/
void uart_txCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount)
{
	(void)handle;
	(void)transferStatus;
	(void)data;
	(void)transferCount;

	if(transferStatus == ECODE_EMDRV_UARTDRV_OK)
	{
		if((!txBuffer.overflow) && (txBuffer.read == txBuffer.write))
			return;

		txBuffer.read++;
		if(txBuffer.read >= txBuffer.size)
		{
			txBuffer.read = 0;
			txBuffer.overflow = false;
		}
	}
}

void uart_rxCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount)
{
	(void)handle;
	(void)transferStatus;
	(void)data;
	(void)transferCount;

	if(transferStatus == ECODE_EMDRV_UARTDRV_OK)
	{
		rxBuf[rxBufCounter++] = *data;
		//if(*data == '\n')
		if((*data == '\n') && (rxBufCounter > rxBuf[4]))
		{
			char* buf = strstr((char *)rxBuf, "AT+");
			volatile int8_t temp = (int8_t)((uint8_t *)buf - (uint8_t *)rxBuf);
			if(rxBufCounter >= rxBuf[temp + 4] + 5) {
				UARTDRV_FlowControlSet(bleHandle, uartdrvFlowControlOff);
				uart_rxHandler(temp);
				rxBufCounter = 0;
				UARTDRV_FlowControlSet(bleHandle, uartdrvFlowControlOn);
			}
		}
	}

	/* RX the next byte */
	UARTDRV_Receive(bleHandle, &rxByte, 1, uart_rxCallback);
}

/******************** global function *********************/
void uart_init(void)
{
	//txBuffer.data = malloc(EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS * sizeof(txBuffer.data));

	/* uart init */
	UARTDRV_Init_t initData;

	DEFINE_BUF_QUEUE(EMDRV_UARTDRV_MAX_CONCURRENT_RX_BUFS, rxBufferQueueI0);
	DEFINE_BUF_QUEUE(EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS, txBufferQueueI0);

	/* UART init */
	initData.port                 = USART0;
	initData.baudRate             = 38400; //9600;
	initData.portLocationTx       = USART_ROUTELOC0_TXLOC_LOC0;
	initData.portLocationRx       = USART_ROUTELOC0_RXLOC_LOC0;
	initData.stopBits             = usartStopbits1;
	initData.parity               = usartNoParity;
	initData.oversampling         = usartOVS16;
	initData.mvdis                = false;
	initData.fcType               = uartdrvFlowControlHw;
	initData.ctsPort 			  = gpioPortA;
	initData.ctsPin 			  = 2;
	initData.portLocationCts	  = USART_ROUTELOC1_CTSLOC_LOC30;
	initData.rtsPort 			  = gpioPortA;
	initData.rtsPin 			  = 3;
	initData.portLocationRts	  = USART_ROUTELOC1_RTSLOC_LOC30;
	initData.rxQueue              = (UARTDRV_Buffer_FifoQueue_t *)&rxBufferQueueI0;
	initData.txQueue              = (UARTDRV_Buffer_FifoQueue_t *)&txBufferQueueI0;

	UARTDRV_InitUart(bleHandle, &initData);
	UARTDRV_Receive(bleHandle, &rxByte, 1, uart_rxCallback);
}

void uart_enableCallback(BLE_CMD cmd, callback_t fkt)
{
	switch(cmd)
	{
		case SET_BATTERY_SOC:
			callback_batterySOC = fkt;
			break;
		case SET_BATTERY_VOLTAGE:
			callback_batteryVoltage = fkt;
			break;
		case SET_BATTERY_CURRENT:
			callback_batteryCurrent = fkt;
			break;
		case SET_BATTERY_TEMP:
			callback_batteryTemperature = fkt;
			break;
		case SET_PULSE_DATA:
			callback_hrmData = fkt;
			break;
		case SET_SPO2_DATA:
			callback_spo2Data = fkt;
			break;
		case SET_HAS_SKIN_CONTACT:
			callback_skinContact = fkt;
			break;
		case SET_BODY_TEMP:
			callback_bodyTemperature = fkt;
			break;
		case SET_ENV_TEMP:
			callback_environmentTemperatrue = fkt;
			break;
		case SET_ENV_HUMIDITY:
			callback_environmentHumidity = fkt;
			break;
		case SET_GYRO:
			callback_gyro = fkt;
			break;
		case SET_ACCEL:
			callback_accel = fkt;
			break;
		case SET_MAG:
			callback_mag = fkt;
			break;
		case SET_GYRO_TIME:
		case SET_GYRO_LIMIT:
		case SET_ACCEL_TIME:
		case SET_ACCEL_LIMIT:
		case SET_MAG_TIME:
		case SET_MAG_LIMIT:
			break;
		case SET_MPU9250_INT:
			callback_mpu9250Int = fkt;
			break;

		case GET_ERROR_STATUS_BYTE:
			callback_errorStatus = fkt;
			break;
		case GET_RESET:
		case GET_CONNECTION:
		case GET_DEBUG_MODE:
			break;
	}
}

void uart_disableCallback(BLE_CMD cmd)
{
	switch(cmd)
	{
		case SET_BATTERY_SOC:
			callback_batterySOC = NULL;
			break;
		case SET_BATTERY_VOLTAGE:
			callback_batteryVoltage = NULL;
			break;
		case SET_BATTERY_CURRENT:
			callback_batteryCurrent = NULL;
			break;
		case SET_BATTERY_TEMP:
			callback_batteryTemperature = NULL;
			break;
		case SET_PULSE_DATA:
			callback_hrmData = NULL;
			break;
		case SET_SPO2_DATA:
			callback_spo2Data = NULL;
			break;
		case SET_HAS_SKIN_CONTACT:
			callback_skinContact = NULL;
			break;
		case SET_BODY_TEMP:
			callback_bodyTemperature = NULL;
			break;
		case SET_ENV_TEMP:
			callback_environmentTemperatrue = NULL;
			break;
		case SET_ENV_HUMIDITY:
			callback_environmentHumidity = NULL;
			break;
		case SET_GYRO:
			callback_gyro = NULL;
			break;
		case SET_ACCEL:
			callback_accel = NULL;
			break;
		case SET_MAG:
			callback_mag = NULL;
			break;
		case SET_GYRO_TIME:
		case SET_GYRO_LIMIT:
		case SET_ACCEL_TIME:
		case SET_ACCEL_LIMIT:
		case SET_MAG_TIME:
		case SET_MAG_LIMIT:
			break;
		case SET_MPU9250_INT:
			callback_mpu9250Int = NULL;
			break;

		case GET_ERROR_STATUS_BYTE:
			callback_errorStatus = NULL;
			break;
		case GET_RESET:
		case GET_CONNECTION:
		case GET_DEBUG_MODE:
			break;
	}
}

uint16_t uart_readValue(BLE_CMD cmd)
{
	uint16_t data = 0;
	switch(cmd)
	{
		case SET_BATTERY_SOC:
			data = rcvData.battery_soc;
			break;
		case SET_BATTERY_VOLTAGE:
			data = rcvData.battery_voltage;
			break;
		case SET_BATTERY_CURRENT:
			data = rcvData.battery_current;
			break;
		case SET_BATTERY_TEMP:
			data = rcvData.battery_temperature;
			break;
		case SET_PULSE_DATA:
			data = rcvData.pulse;
			rcvData.pulseIsNew = false;
			break;
		case SET_SPO2_DATA:
			data = rcvData.spo2;
			rcvData.spo2IsNew = false;
			break;
		case SET_HAS_SKIN_CONTACT:
			data = rcvData.hasSkinContact;
			rcvData.skinIsNew = false;
			break;
		case SET_BODY_TEMP:
			data = rcvData.skin_temp;
			break;
		case SET_ENV_TEMP:
			data = rcvData.env_temp;
			break;
		case SET_ENV_HUMIDITY:
			data = rcvData.env_humidiy;
			break;
		case SET_GYRO:
		case SET_ACCEL:
		case SET_MAG:
		case SET_GYRO_LIMIT:
		case SET_ACCEL_LIMIT:
		case SET_MAG_LIMIT:
			data = 0;
			break;
		case SET_GYRO_TIME:
			data = rcvData.gyroTime;
			break;
		case SET_ACCEL_TIME:
			data = rcvData.accelTime;
			break;
		case SET_MAG_TIME:
			data = rcvData.magTime;
			break;
		case SET_MPU9250_INT:
			data = rcvData.mpu9250Interrupt;
			break;
		case GET_ERROR_STATUS_BYTE:
			data = (rcvData.errorByte << 8) | rcvData.statusByte;
			break;
		case GET_RESET:
			break;
		case GET_CONNECTION:
			data = rcvData.connected;
			break;
		case GET_DEBUG_MODE:
			data = rcvData.debugMode;
	}

	return data;
}

void uart_getGyroData(uint32_t *timestamp, uint32_t *data)
{
	*timestamp = rcvData.gyroTimestamp_1;
	data[0] = rcvData.gyro[0];
	data[1] = rcvData.gyro[1];
	data[2] = rcvData.gyro[2];
}

void uart_getAccelData(uint32_t *timestamp, uint32_t *data)
{
	*timestamp = rcvData.accelTimestamp_1;
	data[0] = rcvData.accel[0];
	data[1] = rcvData.accel[1];
	data[2] = rcvData.accel[2];
}

void uart_getMagnetData(uint32_t *timestamp, uint32_t *data)
{
	*timestamp = rcvData.magTimestamp_1;
	data[0] = rcvData.mag[0];
	data[1] = rcvData.mag[1];
	data[2] = rcvData.mag[2];
}

void uart_getGyroLimit(uint16_t *data)
{
	data[0] = rcvData.gyroLimit[0];
	data[1] = rcvData.gyroLimit[1];
	data[2] = rcvData.gyroLimit[2];
}

void uart_getAccelLimit(uint16_t *data)
{
	data[0] = rcvData.accelLimit[0];
	data[1] = rcvData.accelLimit[1];
	data[2] = rcvData.accelLimit[2];
}

void uart_getMagnetLimit(uint16_t *data)
{
	data[0] = rcvData.magLimit[0];
	data[1] = rcvData.magLimit[1];
	data[2] = rcvData.magLimit[2];
}

void uart_clearMPU9250Interrupt(void)
{
	rcvData.mpu9250Interrupt = 0;
}

void uart_sendCmd(BLE_CMD cmd, uint16_t *data, uint8_t len)
{
	if(cmd == GET_CONNECTION)
		rcvData.connected = *data;
	else if(cmd == GET_DEBUG_MODE)
		rcvData.debugMode = *data;
	else if(cmd == SET_GYRO_TIME)
		rcvData.gyroTime = *data;
	else if(cmd == SET_ACCEL_TIME)
		rcvData.accelTime = *data;
	else if(cmd == SET_MAG_TIME)
		rcvData.magTime = *data;
	else if(cmd == SET_GYRO_LIMIT){
		rcvData.gyroLimit[0] = data[0];
		rcvData.gyroLimit[1] = data[1];
		rcvData.gyroLimit[2] = data[2];
	}
	else if(cmd == SET_ACCEL_LIMIT){
		rcvData.accelLimit[0] = data[0];
		rcvData.accelLimit[1] = data[1];
		rcvData.accelLimit[2] = data[2];
	}
	else if(cmd == SET_MAG_LIMIT){
		rcvData.magLimit[0] = data[0];
		rcvData.magLimit[1] = data[1];
		rcvData.magLimit[2] = data[2];
	}

	uint8_t tmp[len + 9];
	strcpy((char *)tmp, "AT+C");
	tmp[4] = 2 + 2 * len;;
	tmp[5] = cmd;
	uint8_t j = 6;
	for(uint8_t i = 0; i < len; i++)
	{
		tmp[j++] = (data[i] &0xFF00) >> 8;
		tmp[j++] = (data[i] &0x00FF);
	}
	tmp[4 + tmp[4]] = '\n';
	uart_transmit(tmp, tmp[4] + 5);

	return;
}

int8_t uart_sendUpdate(char cmd, uint8_t *data, uint8_t len)
{
	static uint8_t tmp[255];
	static uint8_t dataLen = 9;

	strcpy((char *)tmp, "AT+U");
	switch (cmd)
	{
		case 'F':
			tmp[4] = len + 1;
			tmp[5] = cmd;
			for(dataLen = 0; dataLen < len; dataLen++)
				 tmp[6 + dataLen] = data[dataLen];
			dataLen = 6 + len;
			tmp[dataLen++] = '\n';
			numOfChunks = 0;
			break;
		case 'U':
			if(len > 240)
				return -1;
			tmp[4] = len + 1;
			tmp[5] = cmd;
			for(dataLen = 0; dataLen < len; dataLen++)
				tmp[6 + dataLen] = data[dataLen];
			dataLen = 6 + len;
			tmp[dataLen++] = '\n';
			numOfChunks++;
			break;
		case 'S':
			update.running = true;
			tmp[4] = 4;
			tmp[5] = cmd;
			tmp[6] = data[1];
			tmp[7] = data[0];
			tmp[8] = '\n';
			numOfChunks = 0;
			break;
	}

	update.answered	= false;

	uart_transmit(tmp, dataLen);

	while(!update.answered)
	{
		USTIMER_Delay(1);
		if((cmd == 'U') && (update.chunk == numOfChunks))
			break;
		if(update.answered)
			break;
	}

	if(cmd == 'F')
		update.running = false;

	return (int8_t)update.answer;
}
