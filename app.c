/***************************************************************************//**
 * @file app.c
 * @brief Silicon Labs Empty Example Project
 *
 * This example demonstrates the bare minimum needed for a Blue Gecko C application
 * that allows Over-the-Air Device Firmware Upgrading (OTA DFU). The application
 * starts advertising after boot and restarts advertising after a connection is closed.
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

/* Bluetooth stack headers */
#include "bg_types.h"
#include "native_gecko.h"
#include "gatt_db.h"

#include "uart.h"

#include "app.h"
#include "app_timer.h"
#include "app_ble_adv.h"

#include "infrastructure.h"
#include "aio_service.h"
#include "accori_service.h"
#include "battery_service.h"
#include "smarko_service.h"

#define ADV_TIMEOUT_MS              30000


#define DEVNAME_NAME_TOO_LONG_ERROR  0x0A
#define DEVNAME_NAME_NOT_FOUND_ERROR 0x0B

#define SYSTEM_ID_BYTES 8

#define DEVNAME_PS_KEY 0x4000

// Connection parameters
#define DEFAULT_CON_PARAM_INTERVAL_MS          50
#define DEFAULT_CON_PARAM_LATENCY               0
#define DEFAULT_CON_PARAM_TIMEOUT_MS         1000

#define CON_PARAM_MAX_MIN_DIFF_MS 20

#define DEFAULT_CON_PARAM_MAX_INTERVAL (DEFAULT_CON_PARAM_INTERVAL_MS / 1.25)
#define DEFAULT_CON_PARAM_MIN_INTERVAL ((DEFAULT_CON_PARAM_INTERVAL_MS - CON_PARAM_MAX_MIN_DIFF_MS) / 1.25)
#define DEFAULT_CON_PARAM_TIMEOUT      (DEFAULT_CON_PARAM_TIMEOUT_MS / 10)

#define UPDATE_CON_PARAM_DELAY_MS 3000



/* Print boot message */
static void bootMessage(struct gecko_msg_system_boot_evt_t *bootevt);

static void devNameChanged(uint8_t connection, uint8array *newValue);

/* Flag for indicating DFU Reset must be performed */
static uint8_t boot_to_dfu = 0;

AppBleGattServerAttributeValue_t AppBleGattServerAttributeValue[] =
{
  /* Terminators. */
  { gattdb_device_name, devNameChanged },
};

AppBleGattServerUserReadRequest_t AppBleGattServerUserReadRequest[] =
{
	{ gattdb_accor_acceleration, accoriServiceAccelerationRead },
	{ gattdb_accor_orientation, accoriServiceOrientationRead },
	{ gattdb_accor_magnet, accoriServiceMagnetRead },
	{ gattdb_accor_accelLimit,  accoriServiceAccelLimitRead},
	{ gattdb_accor_orientLimit,  accoriServiceOrientLimitRead},
	{ gattdb_accor_magnetLimit,  accoriServiceMagnetLimitRead},
	{ gattdb_accor_time,  accoriServiceTimeRead},
	{ gattdb_accor_interrupt,  accoriServiceMPUInterruptRead},
	{ gattdb_aio_digital_in, aioServiceDigitalInRead },
	{ gattdb_battery_level, batteryServiceLevelRead },
	{ gattdb_battery_voltage, batteryServiceVoltageRead },
	{ gattdb_battery_current, batteryServiceCurrentRead },
	{ gattdb_battery_temperature, batteryServiceTemperatureRead },
	{ gattdb_smarko_status, smarkoStatusRead },
};

AppBleGattServerUserWriteRequest_t AppBleGattServerUserWriteRequest[] =
{
	{ gattdb_accor_accelLimit,  accoriServiceAccelLimitWrite},
	{ gattdb_accor_orientLimit,  accoriServiceOrientLimitWrite},
	{ gattdb_accor_magnetLimit,  accoriServiceMagnetLimitWrite},
	{ gattdb_accor_time, accoriServiceTimeWrite},
	{ gattdb_accor_interrupt,  accoriServiceMPUInterruptWrite},
	{ gattdb_aio_digital_reset, aioServiceDigitalWriteReset },
	{ gattdb_smarko_init, smarkoServiceSendInit },
	{ gattdb_smarko_update, smarkoServiceUpdate },
	{ gattdb_smarko_finish, smarkoServiceFinish },
};

AppBleGattServerCharStatus_t AppBleGattServerCharStatus[] =
{
	{ gattdb_accor_acceleration, accoriServiceAccelerationCharStatusChange },
	{ gattdb_accor_orientation, accoriServiceOrientationCharStatusChange },
	{ gattdb_accor_magnet, accoriServiceMagnetCharStatusChange },
	{ gattdb_accor_interrupt,  accoriServiceMPUInterruptCharStatusChange},
	{ gattdb_aio_digital_in, aioServiceDigitalInCharStatusChange },
	{ gattdb_battery_level, batteryServiceLeveCharStatusChange },
	{ gattdb_battery_voltage, batteryServiceVoltagCharStatusChange },
	{ gattdb_battery_current, batteryServiceCurrentCharStatusChange },
	{ gattdb_smarko_status, smarkoStatusServiceCharStatusChange },
};

AppBleGattServerConfirmation_t AppBleGattServerConfirmation[] =
{
	{ 0, NULL }
};

size_t AppBleGattServerAttributeValueSize = COUNTOF(AppBleGattServerAttributeValue);
size_t AppBleGattServerUserReadRequestSize = COUNTOF(AppBleGattServerUserReadRequest);
size_t AppBleGattServerUserWriteRequestSize = COUNTOF(AppBleGattServerUserWriteRequest);
size_t AppBleGattServerConfirmationSize = COUNTOF(AppBleGattServerConfirmation);
size_t AppBleGattServerCharStatusSize = COUNTOF(AppBleGattServerCharStatus);

static void devNameChanged(uint8_t connection, uint8array *newValue)
{
  char devName[DEVNAME_MAX_LEN + 1];

  memcpy(devName, newValue->data, newValue->len);
  devName[newValue->len] = 0;

  gecko_cmd_flash_ps_save(DEVNAME_PS_KEY, strlen(devName), (uint8_t *)devName);

  appBleAdvSetDevName(devName);

  appBleAdvInit();
}

void appBleInit(void)
{
  struct gecko_msg_flash_ps_load_rsp_t *psResp;
  char devName[DEVNAME_MAX_LEN + 1];
  uint8_t systemId[SYSTEM_ID_BYTES];

  //uint32_t uniqueId;
  uint8_t btAddr[6];
  struct gecko_msg_system_get_bt_address_rsp_t *btAddrRsp;

  // Extract unique ID from BT Address
  btAddrRsp = gecko_cmd_system_get_bt_address();
  memcpy(btAddr, btAddrRsp->address.addr, 6);
  //uniqueId = 0xFFFFFF & *((uint32_t*) btAddr);

  // Pad and reverse unique ID to get System ID
  systemId[0] = btAddr[5];
  systemId[1] = btAddr[4];
  systemId[2] = btAddr[3];
  systemId[3] = 0xFF;
  systemId[4] = 0xFE;
  systemId[5] = btAddr[2];
  systemId[6] = btAddr[1];
  systemId[7] = btAddr[0];

  // Check if Persistent Storage contains name already, if not, store it there
  psResp = gecko_cmd_flash_ps_load(DEVNAME_PS_KEY);
  if (psResp->result) {
    // Key not found
    // Concatenate name with unique ID and zero terminate
    snprintf(devName, DEVNAME_MAX_LEN + 1, DEVNAME_DEFAULT_STRING);//, (uniqueId & 0xFFFF));

    gecko_cmd_flash_ps_save(DEVNAME_PS_KEY, strlen(devName), (uint8_t *)devName);
  } else {
    memcpy(devName, psResp->value.data, DEVNAME_MAX_LEN + 1);
  }

  appBleAdvSetDevName(devName);

  gecko_cmd_gatt_server_write_attribute_value(gattdb_device_name,
                                              0,
                                              strlen(devName),
                                              (uint8_t*) devName);

  gecko_cmd_gatt_server_write_attribute_value(gattdb_system_id,
                                              0,
                                              SYSTEM_ID_BYTES,
                                              systemId);

  appBleAdvInit();

  accoriServiceInit();
  aioServiceInit();
  batteryServiceInit();
  smarkoServiceInit();
}

void appBleConnectionClosedEvent(uint8_t connection, uint16_t reason)
{
  appBleAdvStart();

  gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(ADV_TIMEOUT_MS), ADV_TIMEOUT_TIMER, true);

  accoriServiceConnectionClosed();
  aioServiceConnectionClosed();
  batteryServiceConnectionClosed();
  smarkoServiceConnectionClosed();
}

void appBleConnectionOpenedEvent(uint8_t connection, uint8_t bonding)
{
  appBleAdvStop();

  gecko_cmd_hardware_set_soft_timer(0, ADV_TIMEOUT_TIMER, false);

  accoriServiceConnectionOpened();
  aioServiceConnectionOpened();
  batteryServiceConnectionOpened();
  smarkoServiceConnectionOpened();
}

/* Main application */
void appMain(gecko_configuration_t *pconfig)
{
	uint8_t i;
	bool bolVar;
#if DISABLE_SLEEP > 0
  pconfig->sleep.flags = 0;
#endif

  /* Initialize debug prints. Note: debug prints are off by default. See DEBUG_LEVEL in app.h */
  initLog();

  /* Initialize stack */
  gecko_init(pconfig);

  while (1) {
    /* Event pointer for handling events */
    struct gecko_cmd_packet* evt;

    /* if there are no events pending then the next call to gecko_wait_event() may cause
     * device go to deep sleep. Make sure that debug prints are flushed before going to sleep */
    if (!gecko_event_pending()) {
      flushLog();
    }

    /* Check for stack event. This is a blocking event listener. If you want non-blocking please see UG136. */
    evt = gecko_wait_event();

    /* Handle events */
    switch (BGLIB_MSG_ID(evt->header)) {
      /* This boot event is generated when the system boots up after reset.
       * Do not call any stack commands before receiving the boot event.
       * Here the system is set to start advertising immediately after boot procedure. */
      case gecko_evt_system_boot_id:

        bootMessage(&(evt->data.evt_system_boot));
        printLog("boot event - starting advertising\r\n");

    	appBleInit();

    	uart_init();
    	appBleAdvStart();

    	gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(ADV_TIMEOUT_MS), ADV_TIMEOUT_TIMER, true);
        /* Set advertising parameters. 100ms advertisement interval.
         * The first parameter is advertising set handle
         * The next two parameters are minimum and maximum advertising interval, both in
         * units of (milliseconds * 1.6).
         * The last two parameters are duration and maxevents left as default. */
        //gecko_cmd_le_gap_set_advertise_timing(0, 160, 160, 0, 0);

        /* Start general advertising and enable connections. */
        //gecko_cmd_le_gap_start_advertising(0, le_gap_general_discoverable, le_gap_connectable_scannable);
        break;

      case gecko_evt_le_connection_opened_id:

        printLog("connection opened\r\n");
        appBleConnectionOpenedEvent(evt->data.evt_le_connection_opened.connection,
                                      evt->data.evt_le_connection_opened.bonding);

	    bolVar = true;
	    uart_sendCmd(GET_CONNECTION, (uint16_t *)bolVar, 1);

        break;

      case gecko_evt_le_connection_closed_id:

        printLog("connection closed, reason: 0x%2.2x\r\n", evt->data.evt_le_connection_closed.reason);

        /* Check if need to boot to OTA DFU mode */
        if (boot_to_dfu) {
          /* Enter to OTA DFU mode */
          gecko_cmd_system_reset(2);
        } else {
        	appBleConnectionClosedEvent(evt->data.evt_le_connection_closed.connection,
										  evt->data.evt_le_connection_closed.reason);
			bolVar = false;
			uart_sendCmd(GET_CONNECTION, (uint16_t *)bolVar, 1);
          /* Restart advertising after client has disconnected */
          //gecko_cmd_le_gap_start_advertising(0, le_gap_general_discoverable, le_gap_connectable_scannable);
        }
        break;

      /* Events related to OTA upgrading
         ----------------------------------------------------------------------------- */

      /* Check if the user-type OTA Control Characteristic was written.
       * If ota_control was written, boot the device into Device Firmware Upgrade (DFU) mode. */
      case gecko_evt_gatt_server_user_write_request_id:

        if (evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ota_control) {
          /* Set flag to enter to OTA mode */
          boot_to_dfu = 1;
          /* Send response to Write Request */
          gecko_cmd_gatt_server_send_user_write_response(
            evt->data.evt_gatt_server_user_write_request.connection,
            gattdb_ota_control,
            bg_err_success);

          /* Close connection to enter to DFU OTA mode */
          gecko_cmd_le_connection_close(evt->data.evt_gatt_server_user_write_request.connection);
        } else {
  		  for (i = 0; i < AppBleGattServerUserWriteRequestSize; i++) {
  			if ((AppBleGattServerUserWriteRequest[i].charId
  				 == evt->data.evt_gatt_server_characteristic_status.characteristic)
  				&& (AppBleGattServerUserWriteRequest[i].fctn)) {
  			  AppBleGattServerUserWriteRequest[i].fctn(
  				evt->data.evt_gatt_server_characteristic_status.connection,
  				&(evt->data.evt_gatt_server_attribute_value.value));
  			}
  		  }
      	}
        break;

      /* Add additional event handlers as your application requires */

		/* Value of attribute changed from the local database by remote GATT client */
		case gecko_evt_gatt_server_attribute_value_id:
		  for (i = 0; i < AppBleGattServerAttributeValueSize; i++) {
			if ((AppBleGattServerAttributeValue[i].charId
				 == evt->data.evt_gatt_server_attribute_value.attribute)
				&& (AppBleGattServerAttributeValue[i].fctn)) {
			  AppBleGattServerAttributeValue[i].fctn(
			    evt->data.evt_gatt_server_characteristic_status.connection,
				&(evt->data.evt_gatt_server_attribute_value.value));
			}
		  }
		  break;

		/* Indicates the changed value of CCC or received characteristic confirmation */
		case gecko_evt_gatt_server_characteristic_status_id:
		  /* Char status changed */
		  if (evt->data.evt_gatt_server_characteristic_status.status_flags == 0x01) {
			for (i = 0; i < AppBleGattServerCharStatusSize; i++) {
			  if ((AppBleGattServerCharStatus[i].charId
				   == evt->data.evt_gatt_server_characteristic_status.characteristic)
				  && (AppBleGattServerCharStatus[i].fctn)) {
				AppBleGattServerCharStatus[i].fctn(
				  evt->data.evt_gatt_server_characteristic_status.connection,
				  evt->data.evt_gatt_server_characteristic_status.client_config_flags);
			  }
			}
		  }
		  /* Confirmation received */
		  else if ((evt->data.evt_gatt_server_characteristic_status.status_flags == 0x02)
				   /* must be a response to an indication*/
				   && (evt->data.evt_gatt_server_characteristic_status.client_config_flags == 2)) {
			for (i = 0; i < AppBleGattServerConfirmationSize; i++) {
			  if ((AppBleGattServerConfirmation[i].charId
				   == evt->data.evt_gatt_server_characteristic_status.characteristic)
				  && (AppBleGattServerConfirmation[i].fctn)) {
				AppBleGattServerConfirmation[i].fctn(
				  evt->data.evt_gatt_server_characteristic_status.connection);
			  }
			}
		  }
		  break;

		/* This event indicates that a remote GATT client is attempting to read a value of an
		 *  attribute from the local GATT database, where the attribute was defined in the GATT
		 *  XML firmware configuration file to have type="user". */
		case gecko_evt_gatt_server_user_read_request_id:
		  for (i = 0; i < AppBleGattServerUserReadRequestSize; i++) {
			if ((AppBleGattServerUserReadRequest[i].charId
				 == evt->data.evt_gatt_server_user_read_request.characteristic)
				&& (AppBleGattServerUserReadRequest[i].fctn)) {
			  AppBleGattServerUserReadRequest[i].fctn(
					  evt->data.evt_gatt_server_characteristic_status.connection);
			}
		  }
		  break;

      case gecko_evt_hardware_soft_timer_id:
		switch (evt->data.evt_hardware_soft_timer.handle) {

			case ADV_TIMEOUT_TIMER:
				  appBleAdvStart();

				  gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(ADV_TIMEOUT_MS), ADV_TIMEOUT_TIMER, true);
				break;

			case ADV_ALTERNATE_TIMER:
				appBleAdvAlternateEvtHandler();
				break;

			default:
			  break;
		  }
		  break;
      default:
        break;
    }
  }
}

/* Print stack version and local Bluetooth address as boot message */
static void bootMessage(struct gecko_msg_system_boot_evt_t *bootevt)
{
#if DEBUG_LEVEL
  bd_addr local_addr;
  int i;

  printLog("stack version: %u.%u.%u\r\n", bootevt->major, bootevt->minor, bootevt->patch);
  local_addr = gecko_cmd_system_get_bt_address()->address;

  printLog("local BT device address: ");
  for (i = 0; i < 5; i++) {
    printLog("%2.2x:", local_addr.addr[5 - i]);
  }
  printLog("%2.2x\r\n", local_addr.addr[0]);
#endif
}
